const messagesRouter = require('express').Router();

const messagesController = require('../controllers/messages');

// messgaes
messagesRouter.get('/', messagesController.getMessages);
messagesRouter.get('/id/:messageId', messagesController.getMessageByMessageId);
messagesRouter.delete('/id/:messageId', messagesController.deleteMessageByMessageId);

module.exports = messagesRouter;
