const { MongoClient } = require('mongodb');

let db;

const initDb = (dbName, callback) => {
  MongoClient.connect(process.env.DB_URI, { useUnifiedTopology: true }, (err, client) => {
    if (err) {
      return callback(err);
    }
    console.log('db connected');
    db = client.db(dbName);
    return callback(undefined, db);
  });
};

const getDb = () => {
  if (typeof db === 'undefined') {
    throw new Error('db is undefind buddy');
  }
  return db;
};

module.exports = {
  initDb,
  getDb,
};
