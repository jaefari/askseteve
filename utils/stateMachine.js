const StateMachine = require('javascript-state-machine');

const userStatesConfig = {
  init: 'firstName',
  transitions: [
    { name: 'step', from: 'firstName', to: 'birthDate' },
    { name: 'step', from: 'birthDate', to: 'wantsNextBirthday' },
    {
      name: 'step',
      from: 'wantsNextBirthday',
      to: (answer) => {
        if (answer === 'yes') {
          return 'calculateNextBirthday';
        }
        return 'bye';
      },
    },
    { name: 'step', from: 'bye', to: 'done' },
    { name: 'step', from: 'calculateNextBirthday', to: 'done' },
  ],
  data: {
    firstName: '',
    birhDate: '',
  },
};


module.exports = () => new StateMachine(userStatesConfig);
