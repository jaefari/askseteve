const socketIo = require('socket.io');

const createStateMachine = require('./stateMachine');
const stateController = require('../controllers/state');

const io = socketIo.listen(3001);

io.on('connection', (socket) => {
  const userState = createStateMachine();
  socket.emit('replyToClient', 'hi buddy! what is your first name? 🤓');

  socket.on('messageToServer', (msg) => {
    stateController(socket, userState, msg);
  });
});

module.exports = io;
