const daysUntilNextBirthday = (birthday) => {
  const oneDay = 24 * 60 * 60 * 1000;

  const now = new Date();

  const currentYear = now.getFullYear();
  const month = birthday.getMonth();
  const day = birthday.getDate();
  const nextBirthday = new Date(currentYear, month, day);

  if (now.getTime() > nextBirthday.getTime()) {
    nextBirthday.setFullYear(nextBirthday.getFullYear() + 1);
  }
  return Math.round((nextBirthday - now) / oneDay);
};

const shortAnswerChecker = (answer) => {
  const yesses = ['yes', 'yep', 'yeap', '^y+$', 'yeah$', 'certainly$', 'sure', 'why not', 'agree', 'affirmative'];
  const nos = ['no', 'nop', 'nope', 'nah', 'nup', 'negative', '^n+$', 'certainly not', 'yeah nah', 'not today'];

  // todo replace with produced regex after tests

  const yesesRegex = new RegExp(yesses.join('|'), 'gi');
  const nosRegex = new RegExp(nos.join('|'), 'gi');

  if (yesesRegex.test(answer)) {
    return 'yes';
  }
  if (nosRegex.test(answer)) {
    return 'no';
  }
  // return no until adding unsure state
  // return 'unsure';
  return 'no';
};

module.exports = {
  daysUntilNextBirthday,
  shortAnswerChecker,
};
