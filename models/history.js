const { ObjectId } = require('mongodb');
const { getDb } = require('../utils/mongodb');

class History {
  constructor(socketId) {
    this.socketId = socketId;
  }

  save() {
    return getDb().collection('histories').updateOne(
      { socketId: this.socketId },
      { $set: { socketId: this.socketId } },
      { upsert: true },
    );
  }

  addToMessages(type, value) {
    return getDb().collection('histories').updateOne(
      { socketId: this.socketId },
      { $push: { messagesLog: { _id: new ObjectId(), type, value } } },
      { writeConcern: { w: 0 } }, // just a log and I don't care if loose one in million
    );
  }

  addToConfirmedData(type, value) {
    return getDb().collection('histories').updateOne(
      { socketId: this.socketId },
      { $push: { confirmedData: { type, value } } },
    );
  }

  static getMessages(page, itemPerPage) {
    return getDb().collection('histories').aggregate([
      { $skip: itemPerPage * (page - 1) },
      { $limit: itemPerPage },
    ])
      .toArray();
  }

  static getMessageByMessageId(messageId) {
    return getDb().collection('histories').aggregate([
      { $match: { 'messagesLog._id': ObjectId(messageId) } },
      { $project: { messagesLog: 1 } },
      { $unwind: '$messagesLog' },
      { $match: { 'messagesLog._id': ObjectId(messageId) } },
      { $project: { _id: '$messagesLog._id', type: '$messagesLog.type', value: '$messagesLog.value' } },
    ])
      .toArray();
  }

  static deleteMessageByMessageId(messageId) {
    return getDb().collection('histories').updateOne(
      { 'messagesLog._id': ObjectId(messageId) },
      { $pull: { messagesLog: { _id: ObjectId(messageId) } } },
    );
  }
}

module.exports = History;
