/* eslint-disable no-param-reassign */

const chrono = require('chrono-node');

const History = require('../models/history');
const utils = require('../utils/utils');

const stateController = async (socket, userState, msg) => {
  try {
    let reply = '';

    const history = new History(socket.id);
    await history.save();

    switch (userState.state) {
      case 'firstName': {
        userState.firstName = msg;
        reply = 'when is your birhdate?';
        userState.step();
        socket.emit('replyToClient', reply);
        await history.addToMessages('firstName', msg);
        await history.addToConfirmedData('firstName', msg);
        break;
      }
      case 'birthDate': {
        const birthDate = chrono.parseDate(msg);
        if (birthDate !== null) {
          userState.birthDate = birthDate;
          reply = 'ok, now I know your birtdate, do you want to know days until your next birthdate?';
          userState.step();
          await history.addToConfirmedData('birthDate', msg);
        } else {
          reply = 'sorry, I could not understand date of your birthday, try again and change format of date please 😊';
        }
        socket.emit('replyToClient', reply);
        await history.addToMessages('birthDate', msg);
        break;
      }
      case 'wantsNextBirthday': {
        const answer = utils.shortAnswerChecker(msg);
        userState.step(answer);
        stateController(socket, userState);
        await history.addToMessages('wantsNextBirthday', msg);
        break;
      }
      case 'calculateNextBirthday': {
        reply = `days until your next birthday: ${utils.daysUntilNextBirthday(userState.birthDate)} 🎉`;
        userState.step();
        socket.emit('replyToClient', reply);
        break;
      }
      case 'bye': {
        reply = 'GoodBye 👋';
        userState.step();
        socket.emit('replyToClient', reply);
        break;
      }
      default:
        break;
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = stateController;
