const History = require('../models/history');

module.exports.getMessages = (req, res, next) => {
  const itemPerPage = +req.query.itemsPerPage || 5;
  const page = +req.query.page || 1;

  return History.getMessages(page, itemPerPage)
    .then((data) => {
      res.json(data);
      return data;
    })
    .catch((error) => {
      next(error);
      return error;
    });
};

module.exports.getMessageByMessageId = (req, res, next) => {
  return History.getMessageByMessageId(req.params.messageId)
    .then((data) => {
      if (data[0]) {
        res.json(data[0]);
        return data[0];
      }
      const error = new Error('message not found');
      error.status = 404;
      throw error;
    })
    .catch((error) => {
      next(error);
      return error;
    });
};

module.exports.deleteMessageByMessageId = (req, res, next) => {
  return History.deleteMessageByMessageId(req.params.messageId)
    .then((data) => {
      if (data.modifiedCount > 0) {
        const response = { message: 'message deleted' };
        res.json(response);
        return response;
      }
      const error = new Error('message not found');
      error.status = 404;
      throw error;
    })
    .catch((error) => {
      next(error);
      return error;
    });
};
