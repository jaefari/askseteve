/* eslint-disable no-undef */
const { expect } = require('chai');

const { shortAnswerChecker } = require('../utils/utils');

describe('shortAnswerChecker', () => {
  describe('yseses', () => {
    const yessesList = ['yes', 'oh yes!', 'yyyyy', 'agree', 'yes certainly'];

    yessesList.forEach((item) => {
      it(`should return yes for '${item}'`, () => {
        expect(shortAnswerChecker(item)).to.equal('yes');
      });
    });
  });

  describe('nos', () => {
    const nosList = ['nope', 'oh NO!', 'certainly nOt', 'yeah no'];

    nosList.forEach((item) => {
      it(`should return no for '${item}'`, () => {
        expect(shortAnswerChecker(item)).to.equal('no');
      });
    });
  });
});
