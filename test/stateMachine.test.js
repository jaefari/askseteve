/* eslint-disable no-undef */
const { expect } = require('chai');

const createStateMachine = require('../utils/stateMachine');

describe('stateMachine', () => {
  describe('success road until condition', () => {
    let stateMachine;
    before(() => {
      stateMachine = createStateMachine();
    });

    it('should be in firstName at first', () => {
      expect(stateMachine.state).to.equal('firstName');
    });

    it('should go to birthDate from firstName by step()', () => {
      stateMachine.step();
      expect(stateMachine.state).to.equal('birthDate');
    });

    it('should go to wantsNextBirthday from birthDate by step()', () => {
      stateMachine.step();
      expect(stateMachine.state).to.equal('wantsNextBirthday');
    });
  });

  describe('success road after condition', () => {
    let stateMachine;
    beforeEach(() => {
      stateMachine = createStateMachine();
      stateMachine.step();
      stateMachine.step();
    });

    it('should go to calculateNextBirthday from wantsNextBirthday by step(yes)', () => {
      stateMachine.step('yes');
      expect(stateMachine.state).to.equal('calculateNextBirthday');
    });

    it('should go to bye from wantsNextBirthday by step(no)', () => {
      stateMachine.step('no');
      expect(stateMachine.state).to.equal('bye');
    });

    it('should go to done anyway', () => {
      stateMachine.step();
      stateMachine.step();
      expect(stateMachine.state).to.equal('done');
    });
  });
});
