/* eslint-disable no-undef */
const { expect } = require('chai');
const { ObjectId } = require('mongodb');
require('dotenv').config();

const mongodb = require('../utils/mongodb');
const messagesController = require('../controllers/messages');

describe('messages rest controller', () => {
  let db;
  let req;
  let res;
  before((done) => {
    req = {
      params: {
        messageId: '5dc1f5f75a818b40c401aa6b',
      },
      query: {},
    };
    res = {
      json: () => '',
    };

    mongodb.initDb(process.env.DB_TESTDB, (err, database) => {
      db = database;
      done();
    });
  });

  beforeEach(() => {
    return db.collection('histories').deleteMany({})
      .then(() => {
        const dummyData = [
          {
            _id: ObjectId('5dc1f5f65a818b40c401aa6a'),
            socketId: 'Hqs-VZaQkHuiDgU1AAAA',
            messagesLog: [
              {
                _id: ObjectId('5dc1f5f75a818b40c401aa6b'),
                type: 'firstName',
                value: 'ma',
              },
              {
                _id: ObjectId('5dc1f6075a818b40c401aa6d'),
                type: 'birthDate',
                value: '2 days ago',
              },
              {
                _id: ObjectId('5dc1f60d5a818b40c401aa70'),
                type: 'wantsNextBirthday',
                value: 'yes',
              },
            ],
            confirmedData: [
              {
                type: 'firstName',
                value: 'ma',
              },
              {
                type: 'birthDate',
                value: '2 days ago',
              },
            ],
          },
          {
            _id: ObjectId('5dc1f6065a818b40c401aa6c'),
            socketId: 'Hqs-VZaQkHuiDgU1AAAA',
          },
        ];
        return db.collection('histories').insertMany(dummyData);
      });
  });

  it('should get messsages', () => {
    return messagesController.getMessages(req, res, () => {})
      .then((data) => {
        expect(data.length).to.equal(2);
      });
  });

  it('should get messsage by id', () => {
    return messagesController.getMessageByMessageId(req, res, () => {})
      .then((data) => {
        // eslint-disable-next-line no-underscore-dangle
        expect(data._id.toString()).to.equal('5dc1f5f75a818b40c401aa6b');
      });
  });

  it('should delete messsage by id', () => {
    return messagesController.deleteMessageByMessageId(req, res, () => {})
      .then((data) => {
        expect(data).to.not.equal(undefined);
      });
  });
});
