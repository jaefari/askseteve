const express = require('express');

const app = express();
const mongodb = require('./utils/mongodb');
require('./utils/socket');
require('dotenv').config();
const messagesRouter = require('./routes/messages');

app.use('/messages', messagesRouter);

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  const status = err.status || 500;
  const message = err.message || 'something went wrong';
  res.status(status).json({ message });
});

mongodb.initDb(process.env.DB_NAME, (err) => {
  if (err) {
    console.log(err);
  }

  app.listen(3000);
});
